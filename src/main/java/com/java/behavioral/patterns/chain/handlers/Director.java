package com.java.behavioral.patterns.chain.handlers;

import com.java.behavioral.patterns.chain.BaseHandler;
import com.java.behavioral.patterns.chain.RequestType;
import com.java.behavioral.patterns.chain.Request;

public class Director extends BaseHandler {

	@Override
	public void handleRequest(Request request) {
		if(request.getRequestType() == RequestType.CONFERENCE) {
			System.out.println("Directors can approve conferences");
		}else {
			successor.handleRequest(request);
		}
	}
}
