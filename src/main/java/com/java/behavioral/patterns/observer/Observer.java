package com.java.behavioral.patterns.observer;

public abstract class Observer {
	protected Subject subject;
	public abstract void update();
}
