package com.java.behavioral.patterns.observer.clients;

import com.java.behavioral.patterns.observer.Observer;
import com.java.behavioral.patterns.observer.Subject;

public class PhoneClient extends Observer {

	public PhoneClient (Subject subject) {
		this.subject = subject;
		subject.attach(this);
	}

	//Optional
	public void addMessage(String message) {
		subject.setState(message + " - sent from phone");
	}
	
	@Override
	public void update() {
		System.out.println("Phone Stream: " + subject.getState());
	}
}
