package com.java.behavioral.patterns.observer.demo;

import com.java.behavioral.patterns.observer.Subject;
import com.java.behavioral.patterns.observer.clients.PhoneClient;
import com.java.behavioral.patterns.observer.MessageStream;
import com.java.behavioral.patterns.observer.clients.TabletClient;

public class ObserverDemo {

	public static void main(String args[]) {
		Subject subject = new MessageStream();
		
		PhoneClient phoneClient = new PhoneClient(subject);
		TabletClient tabletClient = new TabletClient(subject);

		subject.setState("New Message");

		//Optional
		phoneClient.addMessage("Here is a new message!");
		tabletClient.addMessage("Another new message!");
	}
	
}
